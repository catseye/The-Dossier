Text Adventures of Note
=======================

<!--
SPDX-FileCopyrightText: Chris Pressey, the original author of this work, has dedicated it to the public domain.

SPDX-License-Identifier: CC0-1.0
-->

All entries in this article have been relocated to the
[Text Adventures of Note article in the Some Games of Note collection](Some-Games-of-Note/Text%20Adventures%20of%20Note.md).
