Computer Sports Games of Note
=============================

<!--
SPDX-FileCopyrightText: Chris Pressey, the original author of this work, has dedicated it to the public domain.

SPDX-License-Identifier: CC0-1.0
-->

All entries in this article have been relocated to the
[Some Games of Note](https://catseye.tc/node/Some_Games_of_Note) collection.
