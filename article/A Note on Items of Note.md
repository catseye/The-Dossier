A Note on Items of Note
=======================

<!--
SPDX-FileCopyrightText: In 2024, Chris Pressey, the original author of this work, placed it into the public domain.

SPDX-License-Identifier: CC0-1.0
-->

The contents of this article have been relocated to
[the Further Information section of the README of The Dossier.](../README.md#further-information).
