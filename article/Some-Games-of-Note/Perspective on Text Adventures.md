Perspective on Text Adventures
==============================

<!--
SPDX-FileCopyrightText: In 2024, Chris Pressey, the original author of this work, placed it into the public domain.

SPDX-License-Identifier: CC0-1.0
-->

This article has moved to [Perspective on Text Adventures in The Dossier](../Perspective%20on%20Text%20Adventures.md).
