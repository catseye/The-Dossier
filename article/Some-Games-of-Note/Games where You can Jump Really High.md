Games where You can Jump Really High
====================================

<!--
Copyright (c) 2023 Chris Pressey, Cat's Eye Technologies.

SPDX-License-Identifier: CC-BY-ND-4.0
-->

*   common available for: Arcade
*   schema: Video game

This is a list of arcade video games where you (that is, the protagonist
(that is, the character that the player controls)) can jump really, really high!

### Bomb Jack

![screenshot](https://static.catseye.tc/archive/upload.wikimedia.org/wikipedia%252Fen%252F1%252F1a%252FBombjackarc.png)

*   published by: Tecmo
*   available for: Arcade
*   controls: joystick and 1 button
*   date released: 1984
*   wikipedia: [Bomb Jack](https://en.wikipedia.org/wiki/Bomb_Jack)
*   play online @ [archive.org](https://archive.org/details/bombjack_mame)

I remember seeing it (at a laundromat, I think) while my family was on vacation in Portugal.

You can jump really, really high!  You can also slow down your fall after jumping, and glide
horizontally, by pressing the jump button while falling.

### Pac Land

![screenshot](https://archive.org/serve/arcade_pacland/pacland_screenshot.png)

*   published by: Namco
*   available for: Arcade
*   controls: joystick and 1 button
*   date released: 1984
*   wikipedia: [Pac Land](https://en.wikipedia.org/wiki/Pac_Land)
*   play online @ [archive.org](https://archive.org/details/arcade_pacland)

This was installed at the Winnipeg International Airport at one point.

You can jump really, really high!  This is only after you get the magic boots from the
fairy, however, which let you start a jump before you have even finished the previous
jump and come back down to the ground.

### The Legend of Kage

![screenshot](https://upload.wikimedia.org/wikipedia/en/e/e7/The_Legend_of_Kage_%28screenshot%29.png)

*   published by: Taito
*   available for: Arcade
*   controls: joystick and 2 buttons
*   date released: 1985
*   wikipedia: [The Legend of Kage](https://en.wikipedia.org/wiki/The_Legend_of_Kage)

This was also installed at the Winnipeg International Airport at one point.

I am delighted to say I rediscovered the name of this game, by it coming
back as the first hit for a web search for "arcade game jump high ninja trees".

You can jump really, really high!  This is only to be expected of such an awesome
ninja as yourself.  You can also run at full speed when waist-deep in water,
and so forth.

As of this writing the Internet Archive does not have a playable version of it online.
However, it does have some conversions of it for some home systems:

*   [The Legend of Kage (NES)](https://archive.org/details/kage_nes_2)
*   [The Legend of Kage (C64)](https://archive.org/details/Legend_of_Kage_1986_Imagine_Software)
*   [The Legend of Kage (ZX Spectrum)](https://archive.org/details/zx_Legend_of_Kage_1986_Imagine_Software)
