Some Games of Note
==================

<!--
Copyright (c) 2023 Chris Pressey, Cat's Eye Technologies.

SPDX-License-Identifier: CC-BY-ND-4.0
-->

A collection of collections of entries on games that I consider notable in some way.
For more information, [see below](#about-this-collection).

#### Games of Note

*   [Arcade Games of Note](Arcade%20Games%20of%20Note.md) (9)
*   [8-bit Home Computer Games of Note](8-bit%20Home%20Computer%20Games%20of%20Note.md) (10)
*   [Commodore 64 Games of Note](Commodore%2064%20Games%20of%20Note.md) (10)
*   [Apple II Games of Note](Apple%20II%20Games%20of%20Note.md) (6)
*   [Atari 2600 Games of Note](Atari%202600%20Games%20of%20Note.md) (3)
*   [Sports Video Games of Note](Sports%20Video%20Games%20of%20Note.md) (5)
*   [British TV-Derived Games of Note](British%20TV-Derived%20Games%20of%20Note.md) (5)
*   [Games where You can Jump Really High](Games%20where%20You%20can%20Jump%20Really%20High.md) (3)
*   [Computer Games of Note](Computer%20Games%20of%20Note.md) (7)
*   [Role-Playing Games of Note](Role-Playing%20Games%20of%20Note.md) (10)
*   [Text Adventures of Note](Text%20Adventures%20of%20Note.md) (6)
*   [Point-and-Click Adventures of Note](Point-and-Click%20Adventures%20of%20Note.md) (3)

#### Classic Games

*   [Classic Arcade Games](Classic%20Arcade%20Games.md) (8)
*   [Classic Computer Games](Classic%20Computer%20Games.md) (7)
*   [Classic Text Adventures](Classic%20Text%20Adventures.md) (5)

#### Other

*   [Lost Games](Lost%20Games.md) (5)
*   [Recollected Games](Recollected%20Games.md) (21)
*   [Some Modern Retrogames](Some%20Modern%20Retrogames.md) (4)

### About this Collection

These lists were originally from [The Dossier][], and were split out into here to
permit that collection to evolve in a different direction.

This all comes originally from me having compiled a list of my favourite games
I figured that if [Wouter could have such a page](http://strlen.com/rants/fav_games.html),
then I could too.  I had also been told that I have good taste in video games.

It has since blossomed into a list of lists of games
[of note](A%20Note%20on%20Items%20of%20Note.md), where "game" is broader
than "video game" and "of note" is broader than "favourite".

But there is still a heavy emphasis on 8-bit video or computer games.

Gameplay generally counts more than the quality of the graphics or sound
effects or music, but the latter elements sometimes have their part to play
in making a really notable game.  "Realism" of the game is generally not
a factor.

The lists are often ordered from most impressive (to me) to least impressive.
The order and contents of any list in here are subject to change.

Except for the included screenshots, for which the copyright remains with the
rights holder of the particular software being depicted and are used here
under the auspices of "fair dealing", these articles are (c) 2024 Chris Pressey,
Cat's Eye Technologies, and are distributed under a
[Creative Commons Attribution-NoDerivatives license](https://creativecommons.org/licenses/by-nd/4.0/deed.en).

[The Dossier]: https://catseye.tc/node/The_Dossier
