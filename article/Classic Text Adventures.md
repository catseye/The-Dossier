Classic Text Adventures
=======================

<!--
SPDX-FileCopyrightText: Chris Pressey, the original author of this work, has dedicated it to the public domain.

SPDX-License-Identifier: CC0-1.0
-->

All entries in this article have been relocated to the
[Classic Text Adventures article in the Some Games of Note collection](Some-Games-of-Note/Classic%20Text%20Adventures.md).
