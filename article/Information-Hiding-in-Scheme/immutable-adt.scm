;
; immutable-adt.scm
; Another technique to accomplish information hiding in R5RS Scheme.
;
; SPDX-FileCopyrightText: In 2023, Chris Pressey, the original author of this work, placed it into the public domain.
; SPDX-License-Identifier: Unlicense
; For more information, please refer to <https://unlicense.org/>
;

(define new-stack
  (letrec ( (make-stack (lambda (items)
                          (lambda (op args)
                            (cond
                               ((equal? op 'push)
                                (let* ( (item (car args))
                                        (new-items (cons item items)) )
                                  (make-stack new-items)))
                               ((equal? op 'top)
                                 (car items))
                               ((equal? op 'popped)
                                 (make-stack (cdr items)))
                            ))))
          )
    (lambda () (make-stack '())))
)

;
; A transcript of some sample usage
;
; #;1> (define s (new-stack))
; #;2> (define t (s 'push '(4)))
; #;3> (define u (t 'push '(5)))
; #;4> (u 'top '())
; 5
; #;5> ((u 'popped '()) 'top '())
; 4
