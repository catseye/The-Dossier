Lost game (schema)
==================

<!--
SPDX-FileCopyrightText: In 2024, Chris Pressey, the original author of this work, placed it into the public domain.

SPDX-License-Identifier: CC0-1.0
-->

This is a schema which defines, for now, a few rudimentary things about
what properties Lost Game entries in The Dossier should have.

### seen-on

Platform that the game was seen on.
