Role-playing game (schema)
==========================

<!--
SPDX-FileCopyrightText: In 2024, Chris Pressey, the original author of this work, placed it into the public domain.

SPDX-License-Identifier: CC0-1.0
-->

This is a schema which defines, for now, a few rudimentary things about
what properties Role-playing game entries in The Dossier should have.

### written by

*   optional: true

The author(s) of the game.

### published by

*   optional: true

The entity which published the game.

### available for

*   optional: true

The platform or platforms that the video game is or was available for.

### controls

*   optional: true

What the game is controlled with.

### date released

The date that the game was released.

### license

*   optional: true

The license under which it was released.

### written using

*   optional: true

### wikipedia

*   optional: true

If it has an entry on Wikipedia, a link to that.

### order

*   optional: true
*   multiple: true

Gives the link for where it can be ordered from.

### download

*   optional: true
*   multiple: true

Gives the link for where it can be downloaded from.

### entry

*   optional: true
*   multiple: true

Gives the link for an entry in a video games database.

### video

*   optional: true
*   multiple: true

Gives the link for a video of the game being played.

### book

*   optional: true
*   multiple: true

Gives the link for a book on which the game is based (or vice versa).

### walkthrough

*   optional: true
*   multiple: true

Gives the link for a walkthrough.

### review

*   optional: true
*   multiple: true

Gives the link for a review.

### play online

*   optional: true
*   multiple: true

Multiple may occur.
