#!/usr/bin/env python3

# SPDX-FileCopyrightText: Chris Pressey, the original author of this work, has dedicated it to the public domain.
# For more information, please refer to <https://unlicense.org/>
# SPDX-License-Identifier: Unlicense

import json
import sys
try:
    from urllib import quote
except ImportError:
    from urllib.parse import quote
assert quote


def main(args):
    # Suggested values for base_url:
    # "https://catseye.tc/view/The-Dossier/article/"
    # "https://codeberg.org/catseye/The-Dossier/src/branch/master/article/Some-Games-of-Note/"
    base_url = args[0]
    with open(args[1], 'r') as f:
        data = json.loads(f.read())

    n = {}
    for key, value in data.items():
        assert 'anchor' in value and 'filename' in value
        n[key] = {
            "url": "{}{}#{}".format(
                base_url,
                quote(value['filename']),
                value['anchor'],
            )
        }

    sys.stdout.write(json.dumps(n, indent=4, sort_keys=True))


if __name__ == "__main__":
    main(sys.argv[1:])
