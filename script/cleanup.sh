#!/bin/sh -x

# SPDX-FileCopyrightText: Chris Pressey, the original author of this work, has dedicated it to the public domain.
# For more information, please refer to <https://unlicense.org/>
# SPDX-License-Identifier: Unlicense

ARTICLES=../The-Dossier/article

(cd $ARTICLES && feedmark --output-refdex --output-refdex-single-filename \
                          "20th-Century Visual Artists of Note.md" \
                          "An Esolang Reading List.md" \
                 >../dossier-refdex.json) || exit 1

REFDEXES=dossier-refdex.json

# non-lists
feedmark --input-refdexes=$REFDEXES \
         "$ARTICLES/A Basic Theory of Video Games.md" \
         "$ARTICLES/Blurry Memories of DOS Programming.md" \
         "$ARTICLES/Perspective on Text Adventures.md" \
         "$ARTICLES/The Aesthetics of Esolangs.md" \
         "$ARTICLES/Retrospective on Language Design.md" \
         "$ARTICLES/Befunge Silver Jubilee Retrospective.md" \
         --rewrite-markdown || exit 1

# lists
feedmark --input-refdexes=$REFDEXES \
         --check-against-schema=schema/Book.md \
         "$ARTICLES/An Esolang Reading List.md" \
         --rewrite-markdown || exit 1

# Some Games of Note

ARTICLES=./article/Some-Games-of-Note

(cd $ARTICLES && feedmark --output-refdex --output-refdex-single-filename \
                          *.md \
                 >../../sgon-refdex.json) || exit 1

SGON_REFDEXES=sgon-refdex.json

# lists
feedmark --input-refdex=$SGON_REFDEXES \
         --check-against-schema=schema/Text\ adventure.md \
         "$ARTICLES/Text Adventures of Note.md" \
         "$ARTICLES/Classic Text Adventures.md" \
         "$ARTICLES/Point-and-Click Adventures of Note.md" \
         --rewrite-markdown || exit 1

feedmark --input-refdex=$SGON_REFDEXES \
         --check-against-schema=schema/Video\ game.md \
         "$ARTICLES/8-bit Home Computer Games of Note.md" \
         "$ARTICLES/Some Modern Retrogames.md" \
         "$ARTICLES/Commodore 64 Games of Note.md" \
         "$ARTICLES/Apple II Games of Note.md" \
         "$ARTICLES/Arcade Games of Note.md" \
         "$ARTICLES/Classic Arcade Games.md" \
         "$ARTICLES/Atari 2600 Games of Note.md" \
         "$ARTICLES/British TV-Derived Games of Note.md" \
         "$ARTICLES/Sports Video Games of Note.md" \
         "$ARTICLES/Games where You can Jump Really High.md" \
         --rewrite-markdown || exit 1

feedmark --input-refdex=$SGON_REFDEXES \
         --check-against-schema=schema/Computer\ game.md \
         "$ARTICLES/Classic Computer Games.md" \
         "$ARTICLES/Computer Games of Note.md" \
         --rewrite-markdown || exit 1

feedmark --input-refdex=$SGON_REFDEXES \
         --check-against-schema=schema/Role-playing\ game.md \
         "$ARTICLES/Role-Playing Games of Note.md" \
         --rewrite-markdown || exit 1

feedmark --input-refdex=$SGON_REFDEXES \
         --check-against-schema=schema/Lost\ game.md \
         "$ARTICLES/Lost Games.md" \
         --rewrite-markdown || exit 1

feedmark --input-refdex=$SGON_REFDEXES \
         --check-against-schema=schema/Recollected\ game.md \
         "$ARTICLES/Recollected Games.md" \
         --rewrite-markdown || exit 1
